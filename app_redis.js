
var UAParser = require('ua-parser-js');
var parser = new UAParser();
var ua = '';	// ua variable for keep user-agent data

var express = require('express');
var app = express()
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server);
  
var port = 8088;
  
app.configure(function()
{
	app.use(express.static(__dirname + '/public'));
	
	// keep user agent info
	app.use(function(req, res, next) {
		ua = req.get('User-Agent');
		next();
	});
});

server.listen(port);

var redis = require('redis');

io.configure( function() 
{
	io.set('close timeout', 60*60*24); // 24h time out
});

function SessionController (user, email, group, ip, port, ua) 
{
	// session controller class for storing redis connections
	// this is more a workaround for the proof-of-concept
	// in "real" applications session handling should NOT
	// be done like this
	this.sub = redis.createClient();
	this.pub = redis.createClient();
	
	this.user = user;
	this.email = email;
	this.group = group;
	this.userAddr = ip+":"+port;
	this.userAgent = ua;
}

function SessionControllerOpr (user, pwd, group, ip, port, ua) 
{
	// session controller class for storing redis connections
	// this is more a workaround for the proof-of-concept
	// in "real" applications session handling should NOT
	// be done like this
	if (this.sub !== null) this.sub = redis.createClient();
	if (this.pub !== null) this.pub = redis.createClient();
	this.psub = redis.createClient();
	this.client = redis.createClient();
	this.client2 = redis.createClient();
	
	this.user = user;
	this.passwd = pwd;
	this.oprGroup = group;
	this.userAddr = ip+":"+port;
	this.userAgent = ua;
}

SessionController.prototype.subscribe = function(socket) 
{
	this.sub.on('message', function(channel, message) 
	{
		console.log('sub.on message channel: ' + channel);
		//socket.emit(channel, message);
	});
	
	var current = this;
	
	this.sub.on('subscribe', function(channel, count) 
	{
		console.log('sub.on subscribe channel: ' + channel);
		//var joinMessage = JSON.stringify({action: 'control', user: current.user, email: current.email, group: current.group, clientaddr: current.userAddr, msg: ' joined the channel', ua: current.userAgent });
		//current.publish(joinMessage);
	});
	
	console.log('sub.subscribe()');
	this.sub.subscribe('chat');
};

SessionController.prototype.rejoin = function(socket, message) 
{
	this.sub.on('message', function(channel, message) 
	{
		socket.emit(channel, message);
	});
	var current = this;
	this.sub.on('subscribe', function(channel, count) 
	{
		var rejoin = JSON.stringify({action: 'control', user: current.user, msg: ' rejoined the channel' });
		//current.publish(rejoin);
		var reply = JSON.stringify({action: 'message', user: message.user, msg: message.msg });
		//current.publish(reply);
	});
	
	console.log('sub.subscribe() rejoin')
	this.sub.subscribe('chat');
};

SessionController.prototype.unsubscribe = function() 
{
	this.sub.unsubscribe('chat');
};

SessionController.prototype.publish = function(message) 
{
	var msg = JSON.parse(message);
	
	if (msg.action == 'control')
		this.pub.publish('control.chat', message);
	else
		this.pub.publish('queue.chat', message);
};

SessionController.prototype.destroyRedis = function() 
{
	if (this.sub !== null) this.sub.quit();
	if (this.pub !== null) this.pub.quit();
};

SessionControllerOpr.prototype.keys = function(socket) 
{
	var current = this;
	var channels = new Array();
	
	this.client.keys('queue.chat.*', function (err, replies) 
	{
        console.log(replies.length + ' replies:');
		
        replies.forEach(function (replyKey, i) 
		{
            console.log('    ' + i + ': ' + replyKey);
			
			current.client2.hgetall(replyKey, function(err, replyData) 
			{
				//obj = [];
				// reply is null when the key is missing
				if (replyData != null)
				{
					//console.log(replyData);
					//obj.push(replyData);
					var msg = JSON.parse(JSON.stringify(replyData));
					var userInfo = JSON.stringify({action: 'control', user: msg.User, email: msg.Email, group: msg.Group, clientaddr: msg.IP, msg: msg.Message, ua: msg.Browser, channel: replyKey, status: msg.Status, starttime: msg.StartTime });
				
					console.log('send info: ' + userInfo);
					socket.emit('queue.chat.'+msg.Group+'.*', userInfo);
				}
			});
        });
    });
	
	/*setTimeout(function() 
	{ 
		channels.forEach(function (channelKey, j) 
		{
			console.log('    ' + j + ': ' + channelKey);
			
			//this.pub.publish(channelKey, 'test');
			/*this.client2.hget(channelKey, function(err, replyData) 
			{
				// reply is null when the key is missing
				console.log(replyData.toString());
				
				//var msg = JSON.parse(replyData);
				//var userInfo = JSON.stringify({action: 'control', user: msg.User, email: msg.Email, group: msg.Group, clientaddr: msg.IP, msg: ' existed in channel', ua: msg.Browser, channel: replyKey, status: '1', starttime: msg.StartTime });
				
				//socket.emit('queue.chat.'+msg.Group+'.*', userInfo);
			});
		});
	},500);
	
	this.pub.publish('queue.chat', 'test');*/
};

SessionControllerOpr.prototype.subscribe = function(socket) 
{
	this.psub.on('message', function(channel, message) 
	{
		console.log('sub.on message opr channel: ' + channel);
		//socket.emit(channel, message);
	});
	
	this.psub.on('pmessage', function(pattern, channel, message) 
	{
		console.log('sub.on pmessage opr channel: ' + channel);
		
		// {"action":"control","user":"a","email":"a","group":"group1","clientaddr":"192.168.0.158:56966","msg":" joined the channel",
		//"ua":{"browser":{"name":"Chrome","version":"27.0.1453.110","major":"27"},"engine":{"name":"WebKit","version":"537.36"},
		//"os":{"name":"Mac OS X","version":"10.7.5"},"device":{},"cpu":{}}}
		var msg = JSON.parse(message);
		var userInfo = null;
		
		if (msg.msg.indexOf('join') !== -1) 
		{
			userInfo = JSON.stringify({action: msg.action, user: msg.user, email: msg.email, group: msg.group, clientaddr: msg.clientaddr, msg: msg.msg, ua: msg.ua, channel: channel, status: '1', starttime: msg.starttime });
		}
		else if (msg.msg.indexOf('left') !== -1) 
		{
			console.log('left msg: ' + msg.starttime);
			userInfo = JSON.stringify({action: msg.action, user: msg.user, email: msg.email, group: msg.group, clientaddr: msg.clientaddr, msg: msg.msg, ua: msg.ua, channel: channel, status: '0', starttime: msg.starttime });
		}
		else if (msg.msg.indexOf('operator start chat') !== -1) 
		{
			console.log('opr start chat msg: ' + msg.starttime);
			userInfo = JSON.stringify({action: msg.action, user: msg.user, email: msg.email, group: msg.group, clientaddr: msg.clientaddr, msg: msg.msg, ua: msg.ua, channel: channel, status: '2', starttime: msg.starttime });
		}
		
		if (userInfo != null)
			socket.emit(pattern, userInfo);
	});
	
	var current = this;
	
	this.psub.on('subscribe', function(channel, count) 
	{
		console.log('sub.on subscribe opr channel: ' + channel);
		//var joinMessage = JSON.stringify({action: 'control', user: current.user, email: current.email, group: current.group, clientaddr: current.userAddr, msg: ' joined the channel', ua: current.userAgent });
		//current.publish(joinMessage);
	});
	
	this.psub.on('psubscribe', function(channel, count) 
	{
		console.log('sub.on psubscribe opr channel: ' + channel);
		//var joinMessage = JSON.stringify({action: 'control', user: current.user, email: current.email, group: current.group, clientaddr: current.userAddr, msg: ' joined the channel', ua: current.userAgent });
		//current.publish(joinMessage);
	});
	
	console.log('sub.psubscribe() opr');
	this.psub.psubscribe('queue.chat.' + current.oprGroup + '.*');
};

SessionControllerOpr.prototype.unsubscribe = function() 
{
	this.psub.punsubscribe('queue.chat.*');
};

SessionControllerOpr.prototype.publish = function(message) 
{
	var msg = JSON.parse(message);
	
	if (msg.action == 'control')
		this.pub.publish('control.chat', message);
	else
		this.pub.publish('queue.chat', message);
};

SessionControllerOpr.prototype.destroyRedis = function() 
{
	if (this.psub !== null) this.psub.quit();
	if (this.pub !== null) this.pub.quit();
	if (this.client !== null) this.client.quit();
	if (this.client2 !== null) this.client2.quit();
};


// socket io event listener
io.sockets.on('connection', function (socket) // the actual socket callback
{ 
	console.log('socket id: ' + socket.id);
	var address = socket.handshake.address;

	socket.on('chat', function (data)  // receiving chat messages
	{
		var msg = JSON.parse(data);
		socket.get('sessionController', function(err, sessionController) 
		{
			if (sessionController === null) 
			{
				// implicit login - socket can be timed out or disconnected
				var newSessionController = new SessionController(msg.user, msg.email, msg.group, address.address, address.port, parser.setUA(ua).getResult());
				socket.set('sessionController', newSessionController);
				//newSessionController.rejoin(socket, msg);
			} 
			else 
			{
				var chatTime = new Date().getTime();
				var clientChat = JSON.stringify({action: 'message', user: msg.user, email: msg.email, group: msg.group, clientaddr: address.address+':'+address.port, msg: msg.msg, ua: parser.setUA(ua).getResult(), clientchattime: chatTime });
				sessionController.publish(clientChat);
			}
		});
		// just some logging to trace the chat data
		console.log('chat data: ' + data);
	});

	//console.log(parser.setUA(ua).getResult());
	
	socket.on('getlist', function(data) 
	{
		var msg = JSON.parse(data);
		var sessionController = new SessionControllerOpr(msg.user, msg.pwd, msg.group, address.address, address.port, parser.setUA(ua).getResult());
		socket.set('sessionControllerOpr', sessionController);
		sessionController.keys(socket);
		sessionController.subscribe(socket);
		// just some logging to trace the chat data
		console.log('getlist data: ' + data);
	});
	
	socket.on('opr_chatstart', function(data) 
	{
		var chatStart = new Date().getTime();
		var msg = JSON.parse(data);
		var sessionController = new SessionController(msg.user, msg.email, msg.group, address.address, address.port, parser.setUA(ua).getResult());
		socket.set('sessionController', sessionController);
		//sessionController.keys(socket);
		//sessionController.subscribe(socket);
		// just some logging to trace the chat data
		console.log('getlist data: ' + data);
		
		var joinMessage = JSON.stringify({action: 'control', user: msg.user, email: msg.email, group: msg.group, clientaddr: msg.clientaddr, msg: 'operator start chat with user '+msg.user, ua: parser.setUA(ua).getResult(), starttime: chatStart });
		sessionController.publish(joinMessage);
		
		console.log('publish opr_chatstart msg: ' + joinMessage);
	});

	socket.on('join', function(data) 
	{
		//var joinStart = Math.round(new Date().getTime() / 1000);
		var joinStart = new Date().getTime();
		var msg = JSON.parse(data);
		var sessionController = new SessionController(msg.user, msg.email, msg.group, address.address, address.port, parser.setUA(ua).getResult());
		socket.set('sessionController', sessionController);
		sessionController.subscribe(socket);
		// just some logging to trace the chat data
		console.log('join data: ' + data);
		
		var joinMessage = JSON.stringify({action: 'control', user: msg.user, email: msg.email, group: msg.group, clientaddr: address.address+':'+address.port, msg: ' joined the channel', ua: parser.setUA(ua).getResult(), starttime: joinStart });
		sessionController.publish(joinMessage);
		
		console.log('publish join msg: ' + joinMessage);
	});

	socket.on('disconnect', function()  // disconnect from a socket - might happen quite frequently depending on network quality
	{
		socket.get('sessionController', function(err, sessionController) 
		{
			if (sessionController === null) return;
			sessionController.unsubscribe();
			var leaveMessage = JSON.stringify({action: 'control', user: sessionController.user, email: sessionController.email, group: sessionController.group, clientaddr: sessionController.userAddr, msg: ' left the channel', ua: sessionController.userAgent });
			
			console.log('disconnect: ' + leaveMessage);
			sessionController.publish(leaveMessage);
			sessionController.destroyRedis();
		});
		
		socket.get('sessionControllerOpr', function(err, sessionController) 
		{
			if (sessionController === null) 
				return;
			
			sessionController.unsubscribe();
			var leaveMessage = JSON.stringify({action: 'control', user: sessionController.user, pwd: sessionController.passwd, group: sessionController.oprGroup, clientaddr: sessionController.userAddr, msg: ' left the operator channel', ua: sessionController.userAgent });
			
			console.log('disconnect opr: ' + leaveMessage);
			//sessionController.publish(leaveMessage);
			sessionController.destroyRedis();
		});
	});
});
