
$(document).ready(function()
{
	// initial jquery datatable
	var oTable = $('#tb_queue').dataTable({
		"aoColumnDefs": [{ 
			"bSortable": false, 
			"bSortClasses": false,
			"aTargets": [ -1 ], // <-- gets last column and turns off sorting
			"bDeferRender": true
		}],
		"aaSorting": [], // disable auto init sorting
    });
	
	// show join box
	$('#ask').show();
	$('#ask input#user').focus();

	// join on enter
	$('#ask input').keydown(function(event) {
		if (event.keyCode == 13) {
			$('#ask a').click();
		}
	})

	// join on click
	$('#ask a').click(function() {
		//join($('#ask input#user').val(), $('#ask input#pwd').val());
		getlist($('#ask input#user').val(), $('#ask input#pwd').val());
		
		$('#ask').hide();
		//$('#channel').show();
		$('#channellist').show();
		//$('input#message').focus();
	});
	
	var host = window.location.host.split(':')[0];
	var socket = io.connect('http://' + host);
	
	$('#tb_queue').on('click', '#startChat', function(event) {
    	
    	console.log('name: ' + $(this).attr('data-name'));
		
		socket.emit('opr_chatstart', $.toJSON({action: 'control', user: $(this).attr('data-name'), 
						email: $(this).attr('data-email'), 
						clientaddr: $(this).attr('data-ipaddr'), group: $(this).attr('data-group') }));
		
		//$(this).attr('id', 'alreadyChat');
		//console.log('user status: ' + $(this).parent().parent().find('td span#userStatus').text());
		//$(this).parent().parent().find('td span#userStatus').text('On Chat').removeClass('label-warning').addClass('label-success');
		//$(this).parent().find('span#userStatus').text('2');
		
		var div_channel_tab = $('#channelTab');
		
		div_channel_tab.each(function(index, element) {
            $(this).children().removeClass('active');
        });
		
		div_channel_tab.append(
			'<li class="active"><a href="#'+ $(this).attr('data-name') +'">'+ $(this).attr('data-name') +'</a></li>'
		);
		
		var div_content_tab = $('#contentTab');
		
		div_channel_tab.each(function(index, element) {
            $(this).children().removeClass('active');
        });
		
		div_content_tab.append(
			'<div class="tab-pane active" id="'+ $(this).attr('data-name') +'">\
					<div class="row-fluid" id="user-info">\
						<div class="span3">User:&nbsp;'+$(this).attr('data-name')+'</div>\
						<div class="span3">E-mail:&nbsp;'+$(this).attr('data-email')+'</div>\
						<div class="span3">IP Address:&nbsp;'+$(this).attr('data-ipaddr')+'</div>\
						<div class="span3">Group:&nbsp;'+$(this).attr('data-group')+'</div>\
					</div>\
              		<div id="msgs">\
					  <ul>\
						<li class="message" style="display: none">\
						  <span class="user"></span><span class="message"></span>\
						  <span class="time"></span>\
						</li>\
						<li class="control" style="display: none">\
						  <span class="user"></span>&nbsp;<span class="message"></span>\
						  <span class="time"></span>\
						</li>\
					  </ul>\
					</div>\
					<div id="input">\
					  <form><input type="text" id="message_'+ $(this).attr('data-name') +'" /></form>\
					</div>\
                </div>'
		);
		
		$('#channel').show();
		$('a[href=#'+ $(this).attr('data-name') +']').tab('show');
		
		$('#input input').focus();
		
		
		// new message is send in the input box
	    $('#contentTab form').submit(function(event) {
	      event.preventDefault();
	      var input = $(this).find(':input');
	      var msg = input.val();
	      //socket.emit('chat', $.toJSON({action: 'message', user: name, msg: msg}));
		  console.log($(this).html());
		  console.log('msg: ' + msg);
	      input.val('');
	    });
        
    });
	
	function getlist(name, passwd) 
	{
		//var host = window.location.host.split(':')[0];
		//var socket = io.connect('http://' + host);
		
		var gpname = 'group1';
		
		// handler for callback
		socket.on('queue.chat.'+gpname+'.*', function (msg) 
		{
			console.log('getlist data: ' + msg);
			
			var uInfo = $.evalJSON(msg);
			var label_type = 'label-info';
			var label_status = 'Waiting..';
			
			if (uInfo.msg.indexOf('join') !== -1 || uInfo.msg.indexOf('exist') !== -1)
			{
				if (uInfo.status == '1')
				{
					label_type = 'label-warning';
					label_status = 'Waiting..';
				}
				else if (uInfo.status == '2')
				{
					label_type = 'label-success';
					label_status = 'On Chat';
				}
				else
				{
					label_type = 'label-info';
					label_status = 'undefined';
				}
				
				var ua_browser = '';
				
				if (uInfo.msg.indexOf('join') !== -1)
					ua_browser = uInfo.ua.browser.name;
				else
					ua_browser = uInfo.ua;
				
				
				$('#tb_queue').dataTable().fnAddData( [
					uInfo.user,
					uInfo.msg,
					uInfo.starttime,
					uInfo.clientaddr,
					ua_browser,
					'<span id="userStatus" class="label '+label_type+'">'+label_status+'</span>',
					'<span id="startChat" title="Start Chat" class="badge badge-success" data-start="'+uInfo.starttime+
					'" data-name="'+uInfo.user+'" data-ipaddr="'+uInfo.clientaddr+'" data-group="'+uInfo.group+
					'" data-email="'+uInfo.email+
					'"><i class="icon-comment icon-white"></i></span>'
				]);
				
				
			}
			else if (uInfo.msg.indexOf('left') !== -1)
			{
				$('#tb_queue tr').each(function(index, element) {
					
					console.log('index: ' + index + ', elem: ' + $(this).is(':contains("'+uInfo.clientaddr+'")'));
					
					if ($(this).is(':contains("'+uInfo.clientaddr+'")'))
					{
						$('#tb_queue').dataTable().fnDeleteRow(index-1);
						
						console.log('deleted row ' + index);
					}
                });
			}
			else if (uInfo.msg.indexOf('operator start chat') !== -1)
			{
				$('#tb_queue tr').each(function(index, element) {
					
					console.log('index: ' + index + ', elem: ' + $(this).is(':contains("'+uInfo.clientaddr+'")'));
					
					if ($(this).is(':contains("'+uInfo.clientaddr+'")'))
					{
						$(this).find('td span#userStatus').text('On Chat').removeClass('label-warning').addClass('label-success');
						
						//console.log('elem: ' + $(this).find('td span#userStatus').html());
					}
                });
			}
		});
		
		// get group after authen with user and pwd in DB
		// send join message
		socket.emit('getlist', $.toJSON({ user: name, pwd: passwd, group: gpname }));
	}

	function join(name, passwd) 
	{
		//var host = window.location.host.split(':')[0];
		//var socket = io.connect('http://' + host);
		
		// send join message
		socket.emit('join', $.toJSON({ user: name, pwd: passwd }));
		
		var container = $('div#msgs');
		
		// handler for callback
		socket.on('chat', function (msg) 
		{
			var message = $.evalJSON(msg);
			
			var action = message.action;
			var struct = container.find('li.' + action + ':first');
			
			if (struct.length < 1) 
			{
				console.log("Could not handle: " + message);
				return;
			}
			
			// get a new message view from struct template
			var messageView = struct.clone();
			
			// set time
			messageView.find('.time').text((new Date()).toString("HH:mm:ss"));
			
			switch (action) 
			{
				case 'message': 
					var matches;
					// someone starts chat with /me ... 
					if (matches = message.msg.match(/^\s*[\/\\]me\s(.*)/)) {
						messageView.find('.user').text(message.user + ' ' + matches[1]);
						messageView.find('.user').css('font-weight', 'bold');								
					} 
					// normal chat message
					else 
					{
						messageView.find('.user').text(message.user);
						messageView.find('.message').text(': ' + message.msg);									
					}
					break;
				case 'control': 
					messageView.find('.user').text(message.user);
					messageView.find('.message').text(message.msg);
					messageView.addClass('control');
					break;
			}
			
			// color own user:
			if (message.user == name) messageView.find('.user').addClass('self');
			
			// append to container and scroll
			container.find('ul').append(messageView.show());
			container.scrollTop(container.find('ul').innerHeight());
		});

	    // new message is send in the input box
	    $('#channel form').submit(function(event) 
		{
			  event.preventDefault();
			  var input = $(this).find(':input');
			  var msg = input.val();
			  socket.emit('chat', $.toJSON({action: 'message', user: name, msg: msg}));
			  input.val('');
	    }); 
	} // end join function
	
	//------------------------------------------------------------------------------------------------------//
	
	$('#channel').on('click', '#channelTab a', function(e) {
		e.preventDefault();
		$(this).tab('show');
	})
	
});